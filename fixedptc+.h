/*
 * fixedpt+, a simple fixed point math library for C.
 * Released under the BSD 2-Clause "Simplified" License.
 */

 /*
  * Copyright (c) 2010-2012 Ivan Voras <ivoras@freebsd.org>
  * Copyright (c) 2012 Tim Hartrick <tim@edgecast.com>
  * Copyright (c) 2022 Antonio Spalluto <atnosalt8020@tutanota.com>
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted provided that the following conditions
  * are met:
  * 1. Redistributions of source code must retain the above copyright
  *    notice, this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright
  *    notice, this list of conditions and the following disclaimer in the
  *    documentation and/or other materials provided with the distribution.
  *
  * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  * SUCH DAMAGE.
  */

#ifndef _FIXPC_H_
	#define _FIXPC_H_

#ifndef FIXP_BITS
	#define FIXP_BITS 32
#endif

#include <inttypes.h>

#if FIXP_BITS == 8
	typedef int8_t   fixp;
	typedef int16_t  fixpb;
	typedef uint8_t  ufixp;
	typedef uint16_t ufixpb;
#elif FIXP_BITS == 16
	typedef int16_t  fixp;
	typedef int32_t  fixpb;
	typedef uint16_t ufixp;
	typedef uint32_t ufixpb;
#elif FIXP_BITS == 32
	typedef int32_t  fixp;
	typedef int64_t  fixpb;
	typedef uint32_t ufixp;
	typedef uint64_t ufixpb;
#elif FIXP_BITS == 64
	typedef int64_t     fixp;
	typedef __int128_t  fixpb;
	typedef uint64_t    ufixp;
	typedef __uint128_t ufixpb;
#else
	#error "FIXP_BITS must be equal to 8, 16, 32 or 64"
#endif

#if !defined FIXP_WBITS && defined FIXP_FBITS
	#define FIXP_WBITS (FIXP_BITS - FIXP_FBITS)
#elif defined FIXP_WBITS && !defined FIXP_FBITS
	#define FIXP_FBITS (FIXP_BITS - FIXP_WBITS)
#elif !defined FIXP_WBITS && !defined FIXP_FBITS
	#define FIXP_WBITS 16
	#define FIXP_FBITS (FIXP_BITS - FIXP_WBITS)
#endif

/*
 * Two's complement forces at least 1 bit in the integer part and 
 * thus 63 max fbits.
 * 
 * Keep in mind that most of the costants and functions won't work 
 * with low amount of wbits, so it has been capped at 3 bits 
 * (because pi is the biggest constant).
 */
#define FIXP_WMIN 3
#define FIXP_FMIN 1

#if (FIXP_WBITS != FIXP_BITS - FIXP_FBITS)
	#error "FIXP_WBITS + FIXP_FBITS does not fit in FIXP_BITS"
#endif

#if FIXP_WBITS < FIXP_WMIN
	#error "FIXP_WBITS must be more than 2"
#endif

#if FIXP_FBITS < FIXP_FMIN
	#error "FIXP_FBITS must be more than 0"
#endif

#define FIXP_MAXBITS 64

#define FIXP_LSBIT (fixp)(1)

#define FIXP_NAN    (fixp)(0x8000000000000000>>(FIXP_MAXBITS-FIXP_BITS))
#define FIXP_INF    fixp_add(FIXP_NAN,FIXP_LSBIT)
#define FIXP_NEGINF fixp_add(FIXP_INF,FIXP_LSBIT)

#define FIXP_FMASK ((FIXP_LSBIT << FIXP_FBITS) - 1)
#define FIXP_WMASK (~FIXP_FMASK)

#define int_fixp(I) ((fixpb)(I) << FIXP_FBITS)
#define fixp_int(F) ((F) >> FIXP_FBITS)

/*
 * fixp is meant to be usable in environments without floating point support
 * (e.g. microcontrollers, kernels), so we can't use floating point types
 * directly.
 * 
 * Putting them only in macros will effectively make them optional.
 */
// #define fixp_tofloat(T) ((float)((T)*((float)(1)/(float)(1L << FIXP_FBITS))))
/*
 * Converts every numerical type (double,float,int) into fixp,
 * it multiplies the number with FIXP_ONE=(1*2^FIXP_FBITS as normal int)
 * so it "bitshifts" a floating point number with multiplication of powers of 2.
 */
// #define fixp_rconst(R) ((fixp)((R) * FIXP_ONE))

// Basic operations macros
#define fixp_add(A,B)  ((fixp)((fixpb)(A) + (fixpb)(B)))
#define fixp_sub(A,B)  ((fixp)((fixpb)(A) - (fixpb)(B)))
#define fixp_xmul(A,B) ((fixp)(((fixpb)(A) * (fixpb)(B)) >> FIXP_FBITS))
#define fixp_xdiv(A,B) ((fixp)(((fixpb)(A) << FIXP_FBITS) / (fixpb)(B)))

/* Fixpb, ufixpb, are similar to buffers, mainly used to avoid overflow during 
 * operations (ufixpb mainly to avoid two's complement problems),
 * 
 * ex: 16 bits, 8 wbits and fbits, fixp_xdiv(int_fixp(10)/int_fixp(720))
 * 8 wbits, can contain max 127, but fixp_xdiv and int_fixp operate with 
 * fixpb that can contain till 2^24-1, 
 * at the end they convert the result to fixp,
 *
 * Similar use examples of fixpb ufixpb are present in other functions/macros.
 */

// Math functions macros
#define fixp_fracpart(A) ((fixp)(A) & FIXP_FMASK)
#define fixp_intpart(A)  ((fixp)(A) & FIXP_WMASK)
#define fixp_abs(A)      ((A) < 0 ? -(A) : (A))

/*
 * Constants
 *
 * Defines math constants fractional part, 
 * it has to be a positive of the first 64 fractional bits
 */
#define fixp_mkconst_fpart(A) \
        (fixp)((uint64_t)A >> ((FIXP_MAXBITS-FIXP_BITS)+FIXP_WBITS))

#define FIXP_ONE              ((fixp)1 << FIXP_FBITS)
#define FIXP_HALF_ONE         (FIXP_ONE >> 1)
#define FIXP_TWO              fixp_add(FIXP_ONE,FIXP_ONE)
#define FIXP_PI_FRACPART      fixp_mkconst_fpart(0x243F6A8885A308D3)
#define FIXP_PI               fixp_add(FIXP_PI_FRACPART,int_fixp(3))
#define FIXP_TWO_PI           fixp_xmul(FIXP_PI,FIXP_TWO)
#define FIXP_HALF_PI          fixp_xdiv(FIXP_PI,FIXP_TWO)
#define FIXP_FOURTH_PI        fixp_xdiv(FIXP_PI,int_fixp(4))
#define FIXP_THIRD_PI         fixp_xdiv(FIXP_PI,int_fixp(3))
#define FIXP_SIXTH_PI         fixp_xdiv(FIXP_PI,int_fixp(6))
#define FIXP_PI_POW2_FRACPART fixp_mkconst_fpart(0xDE9E64DF22EF2D24)
#define FIXP_PI_POW2          fixp_add(FIXP_PI_POW2_FRACPART,int_fixp(9))
#define FIXP_E_FRACPART       fixp_mkconst_fpart(0xB7E151628AED2A6A)
#define FIXP_E                fixp_add(FIXP_E_FRACPART,FIXP_TWO)
#define FIXP_SQRT2_FRACPART   fixp_mkconst_fpart(0x6A09E667F3BCC908)
#define FIXP_SQRT2            fixp_add(FIXP_SQRT2_FRACPART,FIXP_ONE)
#define FIXP_SQRT3_FRACPART   fixp_mkconst_fpart(0xBB67AE8584CAA73B)
#define FIXP_SQRT3            fixp_add(FIXP_SQRT3_FRACPART,FIXP_ONE)

void fixp_str(fixp A, char *str, int max_frac);

char* fixp_cstr(const fixp A, const int max_frac);

fixp fixp_mul(fixp A, fixp B);

fixp fixp_div(fixp A, fixp B);

fixp fixp_sqrt(fixp A);

fixp fixp_sin(fixp rad);

fixp fixp_cos(fixp rad);

fixp fixp_tan(fixp rad);

fixp fixp_atan(fixp tan);

fixp fixp_asin(fixp sin);

fixp fixp_acos(fixp cos);

/*
fixp fixp_exp(fixp fp);
*/

/*
fixp fixp_ln(fixp x);
*/

/*
fixp fixp_log(fixp x, fixp base);
*/

/*
fixp fixp_pow(fixp n, fixp exp);
*/

#endif
