# FIXEDPT+

## Index
* [Description](#description)
* [Library Data](#library-data)
* [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Acknowledgements](#acknowledgements)

## Description
Fixedpt+ is a library that implements fixed point calculations.

### Main Goals
* Library that is capable of handling different width fixed point types
* Efficiency, function optimization at compile time is based by the value of FIXP_BITS, [Valgrind cachegrind](https://valgrind.org/docs/manual/cg-manual.html)
 has been used to benchmark and helped to reduce the total instructions number.

### Infos
* It is recommended to read all the comments and understand how this library works, this may help to implement with bitshift optimizations in your programs.

* All math functions will give precision till 15 decimal fractional number.

* Since the number of bits in both cases is relatively low, many complex functions (more complex than div & mul) take a hit on the precision
 of the end result because errors in precision accumulate. This loss of precision can be lessened by increasing the number of
 bits dedicated to the fraction part, but at the loss of range.

* Keep in mind that FIXP_NAN, FIXP_INF, FIXP_NEGINF are normal fixp numbers, so it is up to the programmer to set conditions about infinities and undetermined forms in their programs.

* Keep in mind that most of the costants and functions won't work with low amount of WBITS, (it has been capped at 3 bits because pi is the biggest constant).

* Adding and substracting fixp numbers can be done by using the regular integer operators + and -, but it is recommended to use the ```fixp_add``` and ```fixp_sub```
 function-like macros for overflow safe operations (ex: when a low number of WBITS are used).

### Notes
fixedptc+ is a library derived from the original [fixedptc]( https://sourceforge.net/p/fixedptc/code/ci/default/tree/), it received
 substantial modifications such as:
* New "widths" have been added: 8 and 16 bits.
* General macros improvement.
* Special numbers have been added to help manage the indeterminate and infinities cases.
* All the functions have been rewritten to allow major precision (to the 15th
 fractional number) and include optimizations (at compile time) based on the
 number of bits used.

## Library Data

### Data types
* ```fixp```\
fixed point number
* ```ufixp```\
unisgned fixp

### Macros
* ```FIXP_BITS```\
Determines the number of bits of the data type (its "width"), allowed values are 8, 16, 32, 64.
* ```FIXP_WBITS```\
Determines how many bits are dedicated to the "whole" (integer) part of the number, if only FIXP_WBITS is defined, FIXP_FBITS will automatically be defined,
 minimum value for FIXP_WBITS is 3.
* ```FIXP_FBITS```\
Determines how many bits are dedicated to the "fractional" part of the number, if only FIXP_FBITS is defined, FIXP_WBITS will automatically be defined,
 minimum value for FIXP_FBITS is 1.
* ```FIXPT_LSBIT```\
fixp number with its last bit to 1 (ex: with FIXP_BITS 8 FIXP_LSBIT is 0x01).
* ```FIXP_FMASK```\
fixp number with WBITS to 0 and FBITS to 1 (ex: with FIXP_BITS 16 FIXP_FMASK is 0x00ff).
* ```FIXP_WMASK```\
fixp number with WBITS to 1 and FBITS to 0 (ex: FIXP_WMASK is 0xff00).
* ```FIXP_NAN```\
fixp number, is used for undetermined cases, it is represented by the lowest number (ex: with FIXP_WBITS and FIXP_FBITS both 8, FIXP_NAN is -128.99609375).
* ```FIXP_INF```\
fixp number, is used for infinity cases, it is represented by FIXP_NAN+FIXP_LSBIT (ex: FIXP_INF is -128.9921875).
* ```FIXP_NEGINF```\
fixp number, is used for negative infinity cases, it is represented by FIXP_INF+FIXP_LSBIT (ex: FIXP_NEGINF is -128.98828125).

#### Constants
* ```FIXP_ONE```\
1
* ```FIXP_HALF_ONE```\
1/2
* ```FIXP_TWO```\
2
* ```FIXP_PI_FRACPART```\
fractional part of pi
* ```FIXP_PI```\
pi
* ```FIXP_TWO_PI```\
2pi
* ```FIXP_HALF_PI```\
pi/2
* ```FIXP_FOURTH_PI```\
pi/4
* ```FIXP_THIRD_PI```\
pi/3
* ```FIXP_SIXTH_PI```\
pi/6
* ```FIXP_PI_POW2_FRACPART```\
fractional part of pi^2
* ```FIXP_PI_POW2```\
pi^2
* ```FIXP_E_FRACPART```\
fractional part of e
* ```FIXP_E```\
e
* ```FIXP_SQRT2_FRACPART```\
fractional part of sqrt(2)
* ```FIXP_SQRT2```\
sqrt(2)
* ```FIXP_SQRT3_FRACPART```\
fractional part of sqrt(3)
* ```FIXP_SQRT3```\
sqrt(3)

#### Function-like macros
* ```int_fixp(I)```\
int to fixp
* ```fixp_int(F)```\
fixp to int
* ```fixp_add(A,B)```\
sums 2 fixp numbers
* ```fixp_sub(A,B)```\
subtract 2 fixp numbers, a-b
* ```fixp_xmul(A,B)```\
multiplication between two fixp numbers
* ```fixp_xdiv(A,B)```\
division between two fixp numbers A/B, unsafe, use it when you are totally sure that it won't manage infinities or undetermined forms.
* ```fixp_fracpart(A)```\
removes the integer part from a fixp number
* ```fixp_intpart(A)```\
removes the fractional part from a fixp number
* ```fixp_abs(A)```\
absolute value of a fixp number

### Functions
* ```void fixp_str(fixp A, char *str, int max_frac);```\
 Converts a fixp number (fixp A) into string (char *str), the max_frac parameter specifies how many fractional digits, -1 "default", -2 "all"
 there might be invalid digits outside the specified precisions.
* ```char* fixp_cstr(const fixp A, const int max_frac);```\
 Converts the given fixp number into a string, using a static (non-threadsafe) string buffer.
* ```fixp fixp_mul(fixp A, fixp B);```\
 Multiplication as function.
* ```fixp fixp_div(fixp A, fixp B);```\
 Division as function.
* ```fixp fixp_sqrt(fixp A);```\
 Returns the square root of the given number, or FIXP_NAN in case of error.
* ```fixp fixp_sin(fixp A);```\
 Returns the sine of the given fixp number.
* ```fixp fixp_cos(fixp A);```\
 Returns the sine of the given fixp number.
* ```fixp fixp_tan(fixp A);```\
 Returns the tangent of the given fixp number.
* ```fixp fixp_atan(fixp A);```\
 Returns the arctangent of the given fixp number.
* ```fixp fixp_asin(fixp A);```\
 Returns the arcsine of the given fixp number.
* ```fixp fixp_acos(fixp A);```\
 Returns the arcsine of the given fixp number.

## Installation
1. Clone the repo
```sh
git clone git@gitlab.com:Kails_/fixedptc-plus.git
```

## Usage
Assuming one file needs to be compiled.
```sh
1) gcc -o `executable name` `file name.c` fixedptc+.c -Lfixedpt+ -Wfatal-errors -Werror -DFIXP_BITS=32 -DFIXP_FBITS=24
2) gcc -o `executable name` `file name.c` fixedptc+.c -Lfixedpt+ -Wfatal-errors -Werror -DFIXP_BITS=16 -DFIXP_FBITS=8
```

### How to Config
Configuration is defined only by defining FIXP_BITS, FIXP_WBITS and/or FIXP_FBITS before compilation.

## License
Distributed under the ```BSD 2-Clause "Simplified"``` license

## Acknowledgements
* https://sourceforge.net/p/fixedptc/code/ci/default/tree/
* https://mathonweb.com/help_ebook/appendix.htm#algorithms
* https://valgrind.org/docs/manual/cg-manual.html
