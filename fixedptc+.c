#include "fixedptc+.h"

fixp fixp_mul(fixp A, fixp B)
{
	return (((fixpb)A * (fixpb)B) >> FIXP_FBITS);
}

fixp fixp_div(fixp A, fixp B)
{
	if (B==0 && A>0)
		return FIXP_INF;
	if (B==0 && A<0)
		return FIXP_NEGINF;
	if (A==0)
		return FIXP_NAN;
	
	return (((fixpb)A << FIXP_FBITS) / (fixpb)B);
}

void fixp_str(fixp A, char *str, int max_frac)
{
	int nfrac = 0, slen = 0;
	char tmp[17] = {0};
	fixpb B= (fixp)A;
	ufixpb tmp1;

	if (max_frac == -1)
	{
		#if FIXP_FBITS > 32
			max_frac = 15;
		#elif FIXP_FBITS > 16
			max_frac = 8;
		#else
			max_frac = 4;
		#endif
	}
	else if (max_frac == -2)
		max_frac = 19;

	// negative management, reverse two's complement
	if (B < 0)
	{
		str[slen++] = '-';
		B = (~B)+1;
	}

	// integer part
	tmp1 = fixp_int(B);
	do
	{
		tmp[nfrac++] = '0' + tmp1 % 10;
		tmp1 /= 10;

	} while (tmp1 != 0);

	//integer part added to str
	while (nfrac > 0)
		str[slen++] = tmp[--nfrac];

	//point is added
	str[slen++] = '.';

	//fraction part is added to str
	tmp1 = B;
	do
	{
		tmp1 = fixp_fracpart(tmp1);
		tmp1 = fixp_xmul(tmp1,int_fixp(10));
		str[slen++] = '0' + (fixp_int(tmp1) % 10);
		nfrac++;

	} while (tmp1 != 0 && nfrac < max_frac);

	str[slen] = '\0';
}

char* fixp_cstr(const fixp A, const int max_frac)
{
	static char str[25];

	fixp_str(A, str, max_frac);
	return (str);
}

fixp fixp_sqrt(fixp A)
{
	fixp guess, oldguess;

	//special conditions
	if (A < 0)
		return FIXP_NAN;
	if (A == 0 || A == FIXP_ONE)
		return A;

	// Newton's method
	guess = (A >> 1) + FIXP_HALF_ONE;
	oldguess=0;
	while(1)
	{
		guess = fixp_add(guess,fixp_xdiv(A, guess)) >> 1;
		if(oldguess==guess)
			break;
		oldguess=guess;
	}

	return guess;
}

fixp fixp_sin(fixp rad)
{
	/*
	 * It uses sine taylor series polynomial of degree 15:
	 * 	sin(x) =~ x + (1/3!)*x^3 + (1/5!)*x^5 + ... + (1/15!)*x^15
	 * With numerical coefficients as constants:
	 * 	sin(x) =~ x + c1*x^3 + c2*x^5 + ... + c7*x^15
	 * Simplified to:
	 * 	sin(x) =~ x(1+x^2(c1+x^2(c2+x^2(c3+x^2(c4+x^2(c5+x^2(c6+c7x^2)))))))
	 */

	fixp sin, sign=1;

	fixp coeff[8]=
	{
		FIXP_ONE,
		-fixp_mkconst_fpart(0x2AAAAAAAAAAAAAAA),
		fixp_mkconst_fpart(0x0222222222222222),
		-fixp_mkconst_fpart(0x000D00D00D00D00C),
		fixp_mkconst_fpart(0x00002E3BC74AAD8E),
		-fixp_mkconst_fpart(0x0000006B99159FD5),
		fixp_mkconst_fpart(0x00000000B092309D),
		-fixp_mkconst_fpart(0x0000000000D73F9F)
	};

	// using the periodic properties of trigs,
	// it finds an equivalent angle between 0 and pi/2

	// removes multiples of FIXP_TWO_PI, so that 0<rad<2pi or 0>rad>-2pi
	rad%=FIXP_TWO_PI;

	// removes negatives, now 0<rad<2pi
	if(rad<0)
		rad+=FIXP_TWO_PI;

	// cases for every quadrant, now 0<rad<pi/2
	if(rad>FIXP_HALF_PI && rad<=FIXP_PI)
	{
		rad=FIXP_PI-rad;
	}
	else if(rad>FIXP_PI && rad<=fixp_add(FIXP_PI,FIXP_HALF_PI))
	{
		rad-=FIXP_PI;
		sign=-1;
	}
	else if(rad>fixp_add(FIXP_PI,FIXP_HALF_PI) && rad<=FIXP_TWO_PI)
	{
		rad=FIXP_TWO_PI-rad;
		sign=-1;
	}

	/* In the first quadrant angles symmetric with respect to pi/4,
	 * have sine and cosine are inverted
	 * we can shrink the angle span even more to between 0 and pi/4,
	 * it archieves more precision 
	 * (taylor series are more precise when x tends to 0)
	 */
	if(rad>fixp_xdiv(FIXP_HALF_PI,FIXP_TWO))
	{
		sin=fixp_cos(FIXP_HALF_PI-rad);
	}
	else
	{
		fixp s=fixp_xmul(rad,rad);
	/*
	 * Optimizes the code reducing the polinomyal as precision decreases
	 */
		#if FIXP_FBITS>32
			sin=fixp_xmul(s,coeff[7]);
			sin=fixp_xmul(s,coeff[6]+sin);
			sin=fixp_xmul(s,coeff[5]+sin);
			sin=fixp_xmul(s,coeff[4]+sin);
			sin=fixp_xmul(s,coeff[3]+sin);
			sin=fixp_xmul(s,coeff[2]+sin);
			sin=fixp_xmul(s,coeff[1]+sin);
			sin=fixp_xmul(rad,coeff[0]+sin);
		#elif FIXP_FBITS>16
			sin=fixp_xmul(s,coeff[5]);
			sin=fixp_xmul(s,coeff[4]+sin);
			sin=fixp_xmul(s,coeff[3]+sin);
			sin=fixp_xmul(s,coeff[2]+sin);
			sin=fixp_xmul(s,coeff[1]+sin);
			sin=fixp_xmul(rad,coeff[0]+sin);
		#else
			sin=fixp_xmul(s,coeff[2]);
			sin=fixp_xmul(s,coeff[1]+sin);
			sin=fixp_xmul(rad,coeff[0]+sin);
		#endif
	}
	return sign * sin;
}

fixp fixp_cos(fixp rad)
{
	/*
	 * It uses cosine taylor series polynomial of degree 14,
	 * the rest is the same as fixp_sin.
	 */

	fixp cos, sign=1;

	fixp coeff[8]=
	{
		FIXP_ONE,
		-FIXP_HALF_ONE,
		fixp_mkconst_fpart(0x0AAAAAAAAAAAAAAA),
		-fixp_mkconst_fpart(0x005B05B05B05005B),
		fixp_mkconst_fpart(0x0001A01A01A01A01),
		-fixp_mkconst_fpart(0x0000049F93EDDE27),
		fixp_mkconst_fpart(0x00000008F76C77FC),
		-fixp_mkconst_fpart(0x000000000C9CBA54)
	};

	rad%=FIXP_TWO_PI;

	if(rad<0)
		rad+=FIXP_TWO_PI;

	if(rad>FIXP_HALF_PI && rad<=FIXP_PI)
	{
		rad=FIXP_PI-rad;
		sign=-1;
	}
	else if(rad>FIXP_PI && rad<=fixp_add(FIXP_PI,FIXP_HALF_PI))
	{
		rad-=FIXP_PI;
		sign=-1;
	}
	else if(rad>fixp_add(FIXP_PI,FIXP_HALF_PI) && rad<=FIXP_TWO_PI)
	{
		rad=FIXP_TWO_PI-rad;
	}

	if(rad>fixp_xdiv(FIXP_HALF_PI,FIXP_TWO))
	{
		cos=fixp_sin(FIXP_HALF_PI-rad);
	}
	else
	{
		fixpb s=fixp_xmul(rad,rad);
		#if FIXP_FBITS>32
			cos=fixp_xmul(s,coeff[7]);
			cos=fixp_xmul(s,coeff[6]+cos);
			cos=fixp_xmul(s,coeff[5]+cos);
			cos=fixp_xmul(s,coeff[4]+cos);
			cos=fixp_xmul(s,coeff[3]+cos);
			cos=fixp_xmul(s,coeff[2]+cos);
			cos=fixp_xmul(s,coeff[1]+cos);
			cos=coeff[0]+cos;
		#elif FIXP_FBITS>16
			cos=fixp_xmul(s,coeff[5]);
			cos=fixp_xmul(s,coeff[4]+cos);
			cos=fixp_xmul(s,coeff[3]+cos);
			cos=fixp_xmul(s,coeff[2]+cos);
			cos=fixp_xmul(s,coeff[1]+cos);
			cos=coeff[0]+cos;
		#else
			cos=fixp_xmul(s,coeff[3]);
			cos=fixp_xmul(s,coeff[2]+cos);
			cos=fixp_xmul(s,coeff[1]+cos);
			cos=coeff[0]+cos;
		#endif
	}
	return sign * cos;
}

fixp fixp_tan(fixp rad)
{
	fixp sin=fixp_sin(rad);
	fixp cos=fixp_cos(rad);
	fixp a=fixp_div(sin,cos);
	// special case management
	if(a==FIXP_NAN)
		return 0;
	return a;
}

fixp fixp_atan(fixp tan)
{
	/*
	 * It uses arctan taylor series polynomial of degree 23.
	 * See fixp_sin for more info.
	 */

	fixp atan, sign=1;

	fixp coeff[12]=
	{
		FIXP_ONE,
		-fixp_mkconst_fpart(0x5555555555555555),
		fixp_mkconst_fpart(0x3333333333333333),
		-fixp_mkconst_fpart(0x2492492492492492),
		fixp_mkconst_fpart(0x1C71C71C71C71C71),
		-fixp_mkconst_fpart(0x1745D1745D1745D1),
		fixp_mkconst_fpart(0x13B13B13B13B13B1),
		-fixp_mkconst_fpart(0x1111111111111111),
		fixp_mkconst_fpart(0x0F0F0F0F0F0F0F0F),
		-fixp_mkconst_fpart(0x0D79435E50D79435),
		fixp_mkconst_fpart(0x0C30C30C30C30C30),
		-fixp_mkconst_fpart(0x0B21642C8590B216)
	};

	// special cases management
	if(tan==FIXP_INF)
	{
		return FIXP_HALF_PI;
	}
	else if(tan==FIXP_NEGINF)
	{
		return -FIXP_HALF_PI;
	}
	else if(tan==FIXP_NAN)
	{
		return 0;
	}

	// atan(-x)=-atan(x), now 0<tan<inf
	if(tan<0)
	{
		tan=-tan;
		sign=-1;
	}

	// it uses the complementary angle, atan(x)=pi/2-atan(1/x), now 0<tan<1
	if(tan>FIXP_ONE)
	{
		fixp a=fixp_atan(fixp_xdiv(FIXP_ONE,tan));
		atan= fixp_sub(FIXP_HALF_PI,a);
	}
	// it uses the identity pi/6+atan((sqrt3*x-1)/(sqrt3-x)), now 0<tan<=2-sqrt3
	else if(tan>fixp_sub(FIXP_TWO,FIXP_SQRT3))
	{
		fixp a=fixp_xdiv(FIXP_PI,int_fixp(6));
		fixp b=fixp_sub(fixp_xmul(FIXP_SQRT3,tan),FIXP_ONE);
		fixp c=fixp_add(FIXP_SQRT3,tan);
		fixp d=fixp_atan(fixp_xdiv(b,c));
		atan= fixp_add(a,d);
	}
	else
	{
		fixp s=fixp_xmul(tan,tan);
		#if FIXP_FBITS>32
			atan=fixp_xmul(s,coeff[11]);
			atan=fixp_xmul(s,coeff[10]+atan);
			atan=fixp_xmul(s,coeff[9]+atan);
			atan=fixp_xmul(s,coeff[8]+atan);
			atan=fixp_xmul(s,coeff[7]+atan);
			atan=fixp_xmul(s,coeff[6]+atan);
			atan=fixp_xmul(s,coeff[5]+atan);
			atan=fixp_xmul(s,coeff[4]+atan);
			atan=fixp_xmul(s,coeff[3]+atan);
			atan=fixp_xmul(s,coeff[2]+atan);
			atan=fixp_xmul(s,coeff[1]+atan);
			atan=fixp_xmul(tan,coeff[0]+atan);
		#elif FIXP_FBITS>16
			atan=fixp_xmul(s,coeff[7]);
			atan=fixp_xmul(s,coeff[6]+atan);
			atan=fixp_xmul(s,coeff[5]+atan);
			atan=fixp_xmul(s,coeff[4]+atan);
			atan=fixp_xmul(s,coeff[3]+atan);
			atan=fixp_xmul(s,coeff[2]+atan);
			atan=fixp_xmul(s,coeff[1]+atan);
			atan=fixp_xmul(tan,coeff[0]+atan);
		#else
			atan=fixp_xmul(s,coeff[3]);
			atan=fixp_xmul(s,coeff[2]+atan);
			atan=fixp_xmul(s,coeff[1]+atan);
			atan=fixp_xmul(tan,coeff[0]+atan);
		#endif
	}
	return sign * atan;
}

fixp fixp_asin(fixp sin)
{
	//asin(x)= atan(x/sqrt(1-x^2))
	fixp s=fixp_xmul(sin,sin);
	fixp a=fixp_sub(FIXP_ONE,s);
	fixp b=fixp_div(sin,fixp_sqrt(a));
	return fixp_atan(b);
}

fixp fixp_acos(fixp cos)
{
	//acos(x)= atan(sqrt(1-x^2)/x)
	fixp s=fixp_xmul(cos,cos);
	fixp a=fixp_sub(FIXP_ONE,s);
	fixp b=fixp_div(fixp_sqrt(a),cos);
	return fixp_atan(b);
}

/*
fixp
fixp_exp(fixp fp)
{
	fixp xabs, k, z, R, xp;
	const fixp LN2 = fixp_rconst(0.69314718055994530942);
	const fixp LN2_INV = fixp_rconst(1.4426950408889634074);
	const fixp EXP_P[5] = {
		fixp_rconst(1.66666666666666019037e-01),
		fixp_rconst(-2.77777777770155933842e-03),
		fixp_rconst(6.61375632143793436117e-05),
		fixp_rconst(-1.65339022054652515390e-06),
		fixp_rconst(4.13813679705723846039e-08),
	};

	if (fp == 0)
		return (FIXP_ONE);
	xabs = fixp_abs(fp);
	k = fixp_mul(xabs, LN2_INV);
	k += FIXP_ONE_HALF;
	k &= ~FIXP_FMASK;
	if (fp < 0)
		k = -k;
	fp -= fixp_mul(k, LN2);
	z = fixp_mul(fp, fp);
	// Taylor
	R = FIXP_TWO +
	    fixp_mul(z, EXP_P[0] + fixp_mul(z, EXP_P[1] +
	    fixp_mul(z, EXP_P[2] + fixp_mul(z, EXP_P[3] +
	    fixp_mul(z, EXP_P[4])))));
	xp = FIXP_ONE + fixp_div(fixp_mul(fp, FIXP_TWO), R - fp);
	if (k < 0)
		k = FIXP_ONE >> (-k >> FIXP_FBITS);
	else
		k = FIXP_ONE << (k >> FIXP_FBITS);
	return (fixp_mul(k, xp));
}

fixp
fixp_ln(fixp x)
{
	fixp log2, xi;
	fixp f, s, z, w, R;
	const fixp LN2 = fixp_rconst(0.69314718055994530942);
	const fixp LG[7] = {
		fixp_rconst(6.666666666666735130e-01),
		fixp_rconst(3.999999999940941908e-01),
		fixp_rconst(2.857142874366239149e-01),
		fixp_rconst(2.222219843214978396e-01),
		fixp_rconst(1.818357216161805012e-01),
		fixp_rconst(1.531383769920937332e-01),
		fixp_rconst(1.479819860511658591e-01)
	};

	if (x < 0)
		return (0);
	if (x == 0)
		return 0xffff;

	log2 = 0;
	xi = x;
	while (xi > FIXP_TWO) {
		xi >>= 1;
		log2++;
	}
	f = xi - FIXP_ONE;
	s = fixp_div(f, FIXP_TWO + f);
	z = fixp_mul(s, s);
	w = fixp_mul(z, z);
	R = fixp_mul(w, LG[1] + fixp_mul(w, LG[3]
	    + fixp_mul(w, LG[5]))) + fixp_mul(z, LG[0]
	    + fixp_mul(w, LG[2] + fixp_mul(w, LG[4]
	    + fixp_mul(w, LG[6]))));
	return (fixp_mul(LN2, (log2 << FIXP_FBITS)) + f
	    - fixp_mul(s, f - R));
}

fixp
fixp_log(fixp x, fixp base)
{
	return (fixp_div(fixp_ln(x), fixp_ln(base)));
}

fixp
fixp_pow(fixp n, fixp exp)
{
	if (exp == 0)
		return (FIXP_ONE);
	if (n < 0)
		return 0;
	return (fixp_exp(fixp_mul(fixp_ln(n), exp)));
}
*/
