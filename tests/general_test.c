
/* gcc -o test test.c fixedptc_modified.c -Lfixedpt_modified -DFIXEDPT_BITS=64 -DFIXEDPT_WBITS=8 */
#include <stdio.h>
#include <stdlib.h>

/* This is a pure integer-only test program for fixedptc */

#include "../fixedptc+.h"

void fixedpt_print(fixedpt A)
{
	puts(fixedpt_cstr(A,-2));
}

int main() {

	printf("Using %d-bit precision, %d.%d format\n\n", FIXEDPT_BITS, FIXEDPT_WBITS, FIXEDPT_FBITS);

	puts("fractional sensibility (smallest number): ");
	puts(fixedpt_cstr(1, -2));;
	puts("");

	puts("Here are some tests: ");

	puts("\n\nPI_DECIMALS: ");
	fixedpt_print(FIXEDPT_PI_FRACPART);
	printf("0x%" PRIX64 "",FIXEDPT_PI_FRACPART);

	puts("\n\nPI: ");
	fixedpt_print(FIXEDPT_PI);
	printf("0x%" PRIX64 "",FIXEDPT_PI);

	puts("\n\nPI power 2 decimals");
	fixedpt_print(FIXEDPT_PI_POW2_FRACPART);
	printf("0x%" PRIX64 "",(FIXEDPT_PI_POW2_FRACPART));

	puts("\n\nPI power 2");
	fixedpt_print(FIXEDPT_PI_POW2);
	printf("0x%" PRIX64 "",FIXEDPT_PI_POW2);

	puts("\n\nE_DECIMALS: ");
	fixedpt_print(FIXEDPT_E_FRACPART);
	printf("0x%" PRIX64 "",FIXEDPT_E_FRACPART);

	puts("\n\nE: ");
	fixedpt_print(FIXEDPT_E);
	printf("0x%" PRIX64 "",FIXEDPT_E);

	puts("\n\nSQRT2: ");
	fixedpt_print(FIXEDPT_SQRTWO);
	printf("0x%" PRIX64 "",FIXEDPT_SQRTWO);

	puts("\n\nSQRT2_function: ");
	fixedpt_print(fixedpt_sqrt(FIXEDPT_TWO));
	printf("0x%" PRIX64 "",fixedpt_sqrt(FIXEDPT_TWO));

	puts("\n\nnan: ");
	fixedpt_print(FIXEDPT_NAN);
	printf("0x%" PRIX64 "",FIXEDPT_NAN);

	puts("\n\npi/6: ");
	fixedpt_print(fixedpt_xdiv(FIXEDPT_PI,fixedpt_fromint(6)));
	printf("0x%" PRIX64 "",fixedpt_xdiv(FIXEDPT_PI,fixedpt_fromint(6)));


	fixedpt r=fixedpt_xdiv(FIXEDPT_PI,fixedpt_fromint(4));

	puts("\n\nsin(pi/4): ");
	fixedpt_print(fixedpt_sin(r));
	printf("0x%" PRIX64 "",fixedpt_sin(r));

	puts("\n\ncos(pi/4): ");
	fixedpt_print(fixedpt_cos(r));
	printf("0x%" PRIX64 "",fixedpt_cos(r));

	puts("\n\ntan(pi/4): ");
	fixedpt_print(fixedpt_tan(r));
	printf("0x%" PRIX64 "",fixedpt_tan(r));

	puts("");

	return (0);
}
