
/* gcc -o sin_test sin_test.c fixpc_modified.c -Lfixp_modified -Wfatal-errors -DFIXP_BITS=64 -DFIXP_FBITS=66 -DFIXP_WBITS=67*/

#include <stdio.h>
#include <stdlib.h>
/* This is a pure integer-only test program for fixpc */

#include "../fixedptc+.h"

void fixp_print(fixp A)
{
	puts(fixp_cstr(A,-2));
	printf("0x%016" PRIX64 "\n\n",A);
}

int main() {

	printf("Using %d-bit precision, %d.%d format\n\n", FIXP_BITS, FIXP_WBITS, FIXP_FBITS);

	puts("fractional sensibility (smallest number): ");
	fixp_print(1);

	puts("Here are some tests: ");


	// for(int i=0;i<256;++i){
		// fixp_print(i);
	// }

	puts("0.707106781186547524400844362105");
	fixp a=fixp_sin(FIXP_FOURTH_PI);
	fixp_print(a);


	return (0);
}
