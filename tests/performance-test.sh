#!/bin/bash

gcc -o specific_test specific_test.c fixedptc+.c -Lfixedpt+ -Wfatal-errors -Werror -DFIXP_BITS=32 -DFIXP_FBITS=24
valgrind --tool=cachegrind --cachegrind-out-file=cachegrind_specific_test ./specific_test
cg_annotate --threshold=0.02 cachegrind_specific_test
rm cachegrind_specific_test
